class Dice {
  constructor(random) {
    this.random = random;
  }

  roll() {
    return this.random.nextInt();
  }
}

Dice.withFaces = function withFaces(n) {
  return new Dice({
    nextInt() {
      return Math.floor(Math.random() * (n)) + 1;
    },
  });
}

export default Dice;
