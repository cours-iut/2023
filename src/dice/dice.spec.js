import Dice from './dice'

// tag::test[]
test("rolls a dice and get a 1", () => {
    /* Arrange */
    const randomizer = {
        nextInt: jest.fn(() => 1) 
    };
    const dice = new Dice(randomizer);

    /* Act */
    const result = dice.roll();
   
    /* Assert */
    expect(result).toBe(1);
});
// end::test[]

test("rolls a 6 faced dice and get a value between 1 and 6", () => {
    // Arrange
    const dice = Dice.withFaces(6);

    // Act
    const result = dice.roll();

    // Assert
    expect(result).toBeGreaterThanOrEqual(1);
    expect(result).toBeLessThanOrEqual(6);
});