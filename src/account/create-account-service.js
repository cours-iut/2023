import DBConnection from "./db-connection"

export default class CreateAccountService { 

    dbConnection = new DBConnection("localhost", "5432", "eshop_database")

    createAccount(form) {

        let values = []
        for(let key in form) {
           values.push(`${key} = ${form[key]}`) 
        }

        let query = `insert into account values (${values.join(',')})`
        
        let result = this.dbConnection.execute(query)

        return result;
    }
}