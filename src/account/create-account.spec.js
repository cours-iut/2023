
import CreateAccountServiceWithError from "./create-account-service";

class CreateAccountService extends CreateAccountServiceWithError { 

    constructor(dbConnection) {
        super()
        this.dbConnection = dbConnection 
    }

}

test("should create an account with provided data", () => {
    // Arrange
    const dbConnection = { 
        execute: jest.fn()
    }
    const service = new CreateAccountService(dbConnection)

    // Act
    service.createAccount({
        username: 'jdoe',
        password: 'S3cr3t*',
    })

    // Assert
    expect(dbConnection.execute).toHaveBeenCalled()
})