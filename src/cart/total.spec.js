import Cart from "./cart"
import Item from "./item"

test("total amount in a cart is the sum of its items", () => {
    // Arrange
    const cart = new Cart([
        new Item(1, "Keyboard", 100),
        new Item(2, "Mouse", 50)
    ])

    // Act
    const total = cart.total

    // Assert
    expect(total).toEqual(150)
})

test("an empty cart has a zero total", () => {
    // Arrange
    const cart = new Cart()

    // Act
    const total = cart.total
    
    // Assert
    expect(total).toEqual(0)
})