import Cart from "./cart"
import Item from "./item"

// tag::test[]
test("an item can be added into a cart", () => {
   // Arrange
   const cart = new Cart()

   // Act
   cart.addItem(new Item(1, "Keyboard", 100))

   // Assert
   expect(cart.itemsInCart).toHaveLength(1);
})
// end::test[]

test("multiple items can be added into a cart", () => {
    // Arrange
    const cart = new Cart()

    // Act
    cart.addItem(new Item(1, "Keyboard", 100))
    cart.addItem(new Item(2, "Mouse", 50))

    // Assert
    expect(cart.itemsInCart).toHaveLength(2);
})