export default class Cart { 

    itemsInCart = []

    constructor(initialItems = []) {
        this.itemsInCart = initialItems
    }

    get items() {
        return this.itemsInCart
    }

    get total() {
        return this.itemsInCart.map(item => item.price).reduce((acc, i) => acc + i, 0)
    }

    isEmpty() {
        return this.itemsInCart.length == 0
    }

    addItem(item) {
        this.itemsInCart.push(item)
    }

    removeItem(item) {
        this.itemsInCart = this.itemsInCart.filter(elt => elt.id != item.id)
    }
}
