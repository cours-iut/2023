
/* 
// tag::template[]
test("description du test", () => {
    // Arrange
    const animals = ["Cat", "Dog", "Bird", "Lion", "Elephant", "Ant"]

    // Act
    const removedItems = animals.splice("__TODO__");

    // Assert
    expect(removedItems).toEqual(["__TODO__"])
    expect(animals).not.toEqual(expect.arrayContaining(["__TODO__"]))
})
// end::template[]
 */


// tag::tests[]
test("splice can remove an element", () => {
    // Arrange
    const animals = ["Cat", "Dog", "Bird", "Lion", "Elephant", "Ant"]

    // Act
    const removedItems = animals.splice(0, 1);

    // Assert
    expect(removedItems).toEqual(["Cat"])
    expect(animals).not.toEqual(expect.arrayContaining(["Cat"]))
})

test("splice can add an element", () => {
    // Arrange
    const animals = ["Cat", "Dog", "Bird", "Lion", "Elephant", "Ant"]

    // Act
    const removedItems = animals.splice(3, 0, "Lizard")

    // Assert
    expect(removedItems).toEqual([])
    expect(animals).toEqual(expect.arrayContaining(["Lizard"]))
})

test("splice can replace an element", () => {
    // Arrange
    const animals = ["Cat", "Dog", "Bird", "Lion", "Elephant", "Ant"]

    // Act
    const removedItems = animals.splice(3, 1, "Lizard")

    // Assert
    expect(removedItems).toEqual(["Lion"])
    expect(animals).not.toEqual(expect.arrayContaining(["Lion"]))
    expect(animals).toEqual(expect.arrayContaining(["Lizard"]))
})
// end::tests[]