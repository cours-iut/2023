test.each`
someValue  | expectedResult
${1}       | ${true}
${null}    | ${false}
${"lorem"} | ${true}
${[]}      | ${true}
`("$someValue is considered as $expectedResult", 
    ({someValue, expectedResult}) => {
    // Act
    const result = (someValue ? true : false)

    // Assert
    expect(result).toBe(expectedResult)
})
