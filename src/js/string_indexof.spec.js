/*
// tag::template[]
test('description du test', () => {
    // Arrange
    const helloWorld = "Hello World !"
    
    // Act
    const index = helloWorld.indexOf("__TODO__")

    // Assert
    expect(index).toEqual("__TODO__")
})
// end::template[]
*/


// tag::tests[]
test("should return index of first char of the searched string if found", () => {
    // Arrange 
    const helloWorld = "Hello World !"

    // Act
    const index = helloWorld.indexOf("World")

    // Assert
    expect(index).toBe(6)
})

test("should return -1 if when searched string is not found", () =>  {
     // Arrange 
    const helloWorld = "Hello World !"

    // Act
    const index = helloWorld.indexOf("Universe")

    // Assert
    expect(index).toBe(-1)   
})

test("should return 0 when searched string is empty" , () => {
     // Arrange 
    const helloWorld = "Hello World !"

    // Act
    const index = helloWorld.indexOf("")

    // Assert
    expect(index).toBe(0)   
})

test("should return -1 when string is empty" , () => {
     // Arrange 
    const helloWorld = ""

    // Act
    const index = helloWorld.indexOf("World")

    // Assert
    expect(index).toBe(-1)   
})

test("should return -1 when searched string is null" , () => {
     // Arrange 
    const helloWorld = "Hello World !"

    // Act
    const index = helloWorld.indexOf(null)

    // Assert
    expect(index).toBe(-1)   
})

test("should return index of 'null' when searched string is null but string contains null" , () => {
     // Arrange 
    const specialString = "Is this a nullable variable ?"

    // Act
    const index = specialString.indexOf(null)

    // Assert
    expect(index).toBe(10)   
})
// end::tests[]