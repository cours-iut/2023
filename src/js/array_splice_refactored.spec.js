// tag::refactoring_assertion_before[]
test("splice can replace an element", () => {
    // Arrange
    const animals = ["Cat", "Dog", "Bird", "Lion", "Elephant", "Ant"]

    // Act
    const removedItems = animals.splice(3, 1, "Lizard")

    // Assert
    expect(removedItems).toEqual(["Lion"])
    expect(animals).not.toEqual(expect.arrayContaining(["Lion"]))
    expect(animals).toEqual(expect.arrayContaining(["Lizard"]))
})
// end::refactoring_assertion_before[]

// tag::refactoring_assertion_after[]
test("splice can replace an element", () => {
    // Arrange
    const animals = ["Cat", "Dog", "Bird", "Lion", "Elephant", "Ant"]

    // Act
    const removedItems = animals.splice(3, 1, "Lizard")

    // Assert
    expect(removedItems).toEqual(["Lion"])
    expectArray(animals, {
        removedItems: ["Lion"],
        addedItems: ["Lizard"],
    })
})
// end::refactoring_assertion_after[]

// tag::extracted_assertion[]
function expectArray(actual, {removedItems, addedItems}) {
    expect(actual).not.toEqual(expect.arrayContaining(removedItems))
    expect(actual).toEqual(expect.arrayContaining(addedItems))
}
// end::extracted_assertion[]