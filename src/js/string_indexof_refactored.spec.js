

test.each`
string                  | searchedString | expectedIndex
${"Hello World !"}      | ${"World"}     | ${6}
${"Hello World !"}      | ${"Universe"}  | ${-1}
${"Hello World !"}      | ${""}          | ${0}
${""}                   | ${"World"}     | ${-1}
${"Hello World !"}      | ${null}        | ${-1}
${"Is this nullable !"} | ${null}        | ${8}
`("should return $expectedIndex when searching for $searchedString in $string", ({string, searchedString, expectedIndex}) => {
    // Act
    const index = string.indexOf(searchedString)

    // Assert
    expect(index).toBe(expectedIndex)
})
