
const SUNDAY = 0
const SATURDAY = 6

export default class WeekendChecker {

    check() {
        const today = new Date(Date.now())

        if(today.getDay() === SUNDAY || today.getDay() === SATURDAY) {
            return "Yes !"
        }

        return "No :("
    }
}