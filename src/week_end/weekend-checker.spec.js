import WeekendChecker from "./weekend-checker";

function withFakeTime(date, testFunction) {
    return () => {
        // Save original 'now' function
        const now = Date.now

        date.setHours(12, 0, 0)

        // Setup mock
        Date.now = jest.fn(() => date.getTime())

        // Run test
        testFunction()

        // Restore original 'now' function
        Date.now = now
    }
}

test("today is the weekend", withFakeTime(
    new Date(2023, 0, 1),
    () => {
        // Arrange
        const weekendChecker = new WeekendChecker()

        // Act
        const response = weekendChecker.check()

        // Assert
        expect(response).toEqual("Yes !")
    })
)

test("today is not the weekend", withFakeTime(
    new Date(2023, 0, 2),
    () => {
        // Arrange
        const weekendChecker = new WeekendChecker()

        // Act
        const response = weekendChecker.check();

        // Assert
        expect(response).toEqual("No :(")
    })
)