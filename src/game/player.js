// Available classes
const WARRIOR = 1;
const MAGE = 2;
const ROGUE = 3;
const DRUID = 4;

class Player {
  constructor(name, classe) {
    this.name = name;
    this.classe = classe;
  }

  canEquip(arme) {
    if (this.classe === WARRIOR || this.classe === ROGUE) {
      return arme.requiredClasse === WARRIOR || arme.requiredClasse === ROGUE;
    }
    return arme.requiredClasse === this.classe;
  }
}

export default Player;
export { WARRIOR, MAGE, ROGUE, DRUID };
