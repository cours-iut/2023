import Player, { MAGE, WARRIOR } from "./player"
import Weapon from "./weapon"

test("can equip a warrior weapon", () => {
    // Arrange
    const kevin67 = new Player("Kevin67", WARRIOR)
    const twoHandedAxe = new Weapon("Two Handed Axe", WARRIOR)

    // Act
    const canEquip = kevin67.canEquip(twoHandedAxe)

    // Assert
    expect(canEquip).toEqual(true)
})

test("can equip a warrior weapon", () => {
    // Arrange
    const kevin67 = new Player("Kevin67", WARRIOR)
    const fireScepter = new Weapon("Fire Scepter", MAGE)

    // Act
    const canEquip = kevin67.canEquip(fireScepter)

    // Assert
    expect(canEquip).toEqual(false)
})