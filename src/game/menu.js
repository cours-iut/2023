// tag::content[]
import SocialAPI from "./social-api";
import { game } from "./game";

export default class Menu {

    social = new SocialAPI()

    sendScreenshot(message) {
        const screenshot = game.takeScreenshot()
        this.social.send(message, { image: screenshot });
    }

    sendStatus(message) {
        this.social.send(message)
    }
}
// end::content[]

class RefactoredMenu { 

    constructor(social, game) {
        this.social = social
        this.game = game
    }

    sendScreenshot(message) {
        const screenshot = this.game.takeScreenshot()
        this.social.send(message, { image: screenshot });
    }

    sendStatus(message) {
        this.social.send(message)
    }
}

export { RefactoredMenu }