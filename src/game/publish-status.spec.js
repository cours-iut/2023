import CoupledMenu, { RefactoredMenu as Menu } from "./menu";

// Override Menu to fix coupling and make it testable
class MenuForTest extends CoupledMenu {
  constructor(socialAPI, game) {
    super();
    this.social = socialAPI;
    this.game = game;
  }

  sendScreenshot(message) {
    const screenshot = this.game.takeScreenshot();
    this.social.send(message, { image: screenshot });
  }
}

describe("Using the refactored Menu", () => {
  // tag::tests[]
  test("a user can publish a status on social network", () => {
    // Arrange
    const socialAPI = {
      send: jest.fn(() => {}),
    };
    const menu = new Menu(socialAPI, {});

    // Act
    menu.sendStatus("Venez me rejoindre !");

    // Assert
    expect(socialAPI.send).toHaveBeenNthCalledWith(1, "Venez me rejoindre !");
  });

  test("a user can publish a status and a screenshot on social network", () => {
    // Arrange
    const socialAPI = {
      send: jest.fn(() => {}),
    };
    const game = {
      takeScreenshot: jest.fn(() => "mocked screenshot"),
    };
    const menu = new Menu(socialAPI, game);

    // Act
    menu.sendScreenshot("Admirez ce paysage !");

    // Assert
    expect(socialAPI.send).toHaveBeenNthCalledWith(1, "Admirez ce paysage !", {
      image: "mocked screenshot",
    });
  });
  // end::tests[]
});

describe("Using an overridden Menu for tests", () => {
  test("a user can publish a status on social network", () => {
    // Arrange
    const socialAPI = {
      send: jest.fn(() => {}),
    };
    const menu = new MenuForTest(socialAPI, {});

    // Act
    menu.sendStatus("Venez me rejoindre !");

    // Assert
    expect(socialAPI.send).toHaveBeenNthCalledWith(1, "Venez me rejoindre !");
  });

  test("a user can publish a status and a screenshot on social network", () => {
    // Arrange
    const socialAPI = {
      send: jest.fn(() => {}),
    };
    const game = {
      takeScreenshot: jest.fn(() => "mocked screenshot"),
    };
    const menu = new MenuForTest(socialAPI, game);

    // Act
    menu.sendScreenshot("Admirez ce paysage !");

    // Assert
    expect(socialAPI.send).toHaveBeenNthCalledWith(1, "Admirez ce paysage !", {
      image: "mocked screenshot",
    });
  });
});
