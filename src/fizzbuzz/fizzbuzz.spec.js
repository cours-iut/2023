import fizzbuzz from "./fizzbuzz"

test("return fizz when multiple of three", () => {
    // Arrange
    
    // Act
    const result = fizzbuzz(3)

    // Assert
    expect(result).toEqual("Fizz")
})

test("return Buzz when multiple of five", () => {
    // Arrange

    // Act
    const result = fizzbuzz(5)

    // Assert
    expect(result).toEqual("Buzz")
})


test("return FizzBuzz when multiple of three and five", () => {
    // Arrange

    // Act
    const result = fizzbuzz(15)

    // Assert
    expect(result).toEqual("FizzBuzz")
})

test("return given integer when not multiple of three or five", () => {
    // Arrange

    // Act
    const result = fizzbuzz(7)

    // Assert
    expect(result).toEqual("7")
})