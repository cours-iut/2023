
export default function fizzbuzz(i) {
    return i % 15 == 0 ? "FizzBuzz"
        : i % 5 == 0 ? "Buzz"
        : i % 3 == 0 ? "Fizz"
        : `${i}`
}