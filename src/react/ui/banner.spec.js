import { render } from "@testing-library/react";
import renderer from "react-test-renderer";
import Banner from "./banner";

test("should match snapshot", () => {
  // Arrange

  // Act
  const result = renderer.create(<Banner>Some Awesome Title</Banner>).toJSON();

  // Assert
  expect(result).toMatchSnapshot();
});

test("should print title", () => {
    // Arrange

    // Act
    const { getByTestId } = render(<Banner>Some Awesome Title</Banner>)

    // Assert
    expect(getByTestId('title').innerHTML).toEqual("Some Awesome Title")
})
