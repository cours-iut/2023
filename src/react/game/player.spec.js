import { render } from "@testing-library/react";
import React from "react";
import renderer from "react-test-renderer";
import Player from "./player";

const weaponFactory = {
  createSword() {
    return {
      id: "1e8d",
      name: "Epée courte",
      requiredClass: "Guerrier",
      description: "Maniable et équilibrée.",
    };
  },
};

// tag::snapshot[]
test("match snapshot", () => {
  /* Arrange */
  const player = {
    name: "kevinDu67",
    classe: "Guerrier",
    bag: [weaponFactory.createSword()],
  };

  /* Act */
  const result = renderer.create(<Player player={player} />)
    .toJSON();

  /* Assert */
  expect(result).toMatchSnapshot();
});
// end::snapshot[]

// tag::data_attribute[]
test("affiche la classe du joueur", () => {
  /* Arrange */
  const player = {
    name: "kevinDu67",
    classe: "Guerrier",
    bag: [weaponFactory.createSword()],
  };

  /* Act */
  const { getByTestId } = render(<Player player={player} />);

  /* Assert */
  expect(getByTestId("player-class").innerHTML)
    .toEqual(expect.stringContaining("Guerrier"));
});
// end::data_attribute[]

test("affiche l'inventaire", () => {
  // Arrange
  const sword = {
    id: "1e8d",
    name: "Epée courte",
    requiredClass: "Guerrier",
    description: "Maniable et équilibrée.",
  };
  const dagger = {
    id: "f52e",
    name: "Dague rituelle",
    requiredClass: "Voleur",
    description: "Une courte dague effilée",
  };
  const player = {
    name: "kevinDu67",
    classe: "Guerrier",
    bag: [sword, dagger],
  };

  // Act
  const { getAllByTestId } = render(<Player player={player} />);
  const items = getAllByTestId(/bag-weapon-.*/);

  // Assert
  expect(items.length).toEqual(2);

  const ids = items
    .map((it) => it.attributes)
    .map((attrs) => attrs["data-testid"])
    .map((attr) => attr.value)
    .map((testId) => testId.replace("bag-weapon-", ""));
  expect(ids).toEqual(["1e8d", "f52e"]);
});
