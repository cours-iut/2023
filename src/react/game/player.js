import React from 'react';
import EquipWeapon from './equip-weapon';

function Player({ player, onSelectWeapon }) {
  const { name, classe, equippedWeapon, bag } = player;
  return (
    <div>
      {/* tag::content[] */}
      <h1>{name}</h1> 
      <h3 data-testid="player-class">{classe}</h3>
      <hr/>
      <ul>
        {bag.map((weapon) => ( 
            <li key={weapon.id} data-testid={`bag-weapon-${weapon.id}`}>
              <EquipWeapon
                active={equippedWeapon === weapon.id}
                onClick={() => onSelectWeapon(weapon)}
              />
              ({weapon.classe}) {weapon.nom} <em>{weapon.description}</em>
            </li>
          )
        )}
      </ul>
      {/* end::content[] */}
    </div>
  );
}

export default Player;