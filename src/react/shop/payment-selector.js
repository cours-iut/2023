// tag::content[]
import { useEffect, useState } from "react";

export default function PaymentSelector({ userId, onPaymentMethodSelected }) {
  const [paymentMethods, setPaymentMethods] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch(`/api/user/${userId}/payment-methods`)
      .then((response) => response.json())
      .then((json) => {
        setPaymentMethods(json.paymentMethods);
        setSelectedMethod(json.paymentMethods[0].code);
        setLoading(false);
      });
  });

  if (loading) {
    return <div>Chargement en cours ...</div>;
  }

  return (
    <div>
      {paymentMethods.length == 0 ? (
        <button>Ajouter un moyen de paiement</button>
      ) : (
        <select onChange={onPaymentMethodSelected}>
          {paymentMethods.map((method) => (
            <option key={method.code} value={method.code}>
              {method.name}
            </option>
          ))}
        </select>
      )}
    </div>
  );
}
// end::content[]

function PaymentSelectorComponent({
  loading,
  paymentMethods,
  onPaymentMethodSelected,
}) {
  if (loading) {
    return (
      <div data-testid="payment-selector-loading">Chargement en cours ...</div>
    );
  }

  if (paymentMethods.length == 0) {
    return (
      <button data-testid="payment-selector-no-methods">
        Ajouter un moyen de paiement
      </button>
    );
  }

  return (
    <div>
      <select onChange={onPaymentMethodSelected}>
        {paymentMethods.map((method) => (
          <option
            key={method.code}
            value={method.code}
            data-testid="payment-selector-method"
          >
            {method.name}
          </option>
        ))}
      </select>
    </div>
  );
}

function PaymentSelectorContainer({ userId, onPaymentMethodSelected }) {
  const [paymentMethods, setPaymentMethods] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch(`/api/user/${userId}/payment-methods`)
      .then((response) => response.json())
      .then((json) => {
        setPaymentMethods(json.paymentMethods);
        setSelectedMethod(json.paymentMethods[0].code);
        setLoading(false);
      });
  });

  return (
    <PaymentSelectorComponent
      loading={loading}
      paymentMethods={paymentMethods}
      onPaymentMethodSelected={onPaymentMethodSelected}
    />
  );
}

export { PaymentSelectorComponent, PaymentSelectorContainer };
