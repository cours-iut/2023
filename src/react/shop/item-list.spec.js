import { render } from '@testing-library/react';
import renderer from 'react-test-renderer';
import { ItemListComponent } from './item-list';

const itemFactory = {
    createItem(name, price) { 
        return { name, price }
    },
    moonlander() { 
        return this.createItem("Moonlander", 320)
    },
    planck() { 
        return this.createItem("Planck", 250)
    },
}

test("snapshot without items", () => {
    // Arrange
    const items = []

    // Act
    const result = renderer.create(<ItemListComponent items={items} />)

    // Assert
    expect(result).toMatchSnapshot();
})


test("snapshot with items", () => {
    // Arrange
    const items = [
        itemFactory.moonlander(),
        itemFactory.planck()
    ]

    // Act
    const result = renderer.create(<ItemListComponent items={items} />)

    // Assert
    expect(result).toMatchSnapshot();
})

test("has no items", () => {
    // Arrange
    const items = []

    // Act
    const { getByTestId, getAllByTestId } = render(<ItemListComponent items={items} />)

    // Assert
    expect(getByTestId('item-list-no-items')).toBeDefined()
})

test("has items", () => {
    // Arrange
    const items = [
        itemFactory.moonlander(),
        itemFactory.planck()
    ]

    // Act
    const { getByTestId, getAllByTestId } = render(<ItemListComponent items={items} />)

    // Assert
    expect(getAllByTestId('item-list-item')).toHaveLength(2)
})