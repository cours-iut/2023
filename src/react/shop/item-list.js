// tag::content[]
import { useEffect, useState } from "react";

export default function ItemList({ title, addToCart }) {
  const [items, setItems] = useState([]);

  useEffect(() => {
    fetch("/api/items")
      .then((response) => response.json())
      .then((json) => setItems(json.items));
  });

  return (
    <div>
      <h2>{title}</h2>
      {items.map((item, index) => (
        <div key={index}>
          <h3>{item.name}</h3>
          <p>{item.price}</p>
          <button onClick={addToCart}>Ajouter dans le panier</button>
        </div>
      ))}
      {items.length == 0 && <p>Aucun article</p>}
    </div>
  );
}
// end::content[]

function ItemListComponent({ title, items, addToCart }) {
  if (items.length == 0) {
    return <p data-testid="item-list-no-items">Aucun article</p>;
  }

  return (
    <div>
      <h2 data-testid="item-list-title">{title}</h2>
      {items.map((item, index) => (
        <div key={index} data-testid="item-list-item">
          <h3>{item.name}</h3>
          <p>{item.price}</p>
          <button onClick={addToCart}>Ajouter dans le panier</button>
        </div>
      ))}
    </div>
  );
}

function ItemListContainer() {
  const [items, setItems] = useState([]);

  useEffect(() => {
    fetch("/api/items")
      .then((response) => response.json())
      .then((json) => setItems(json.items));
  });

  return <ItemListComponent items={items} />;
}

export { ItemListComponent, ItemListContainer };
