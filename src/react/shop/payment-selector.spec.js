import { render } from "@testing-library/react";
import renderer from "react-test-renderer";
import { PaymentSelectorComponent } from "./payment-selector";

const paymentMethodFactory = {
  createMethod(code, name) {
    return { code, name };
  },
  visa() {
    return this.createMethod("visa", "Carte Visa");
  },
};

test("snapshot with payment methods", () => {
  // Arrange
  const paymentMethods = [paymentMethodFactory.visa()];

  // Act
  const result = renderer
    .create(
      <PaymentSelectorComponent
        loading={false}
        paymentMethods={paymentMethods}
      />
    )
    .toJSON();

  // Assert
  expect(result).toMatchSnapshot();
});

test("snapshot without payment method", () => {
  // Arrange
  const paymentMethods = [];

  // Act
  const result = renderer
    .create(
      <PaymentSelectorComponent
        loading={false}
        paymentMethods={paymentMethods}
      />
    )
    .toJSON();

  // Assert
  expect(result).toMatchSnapshot();
});

test("snapshot in loading", () => {
  // Arrange
  const paymentMethods = [];

  // Act
  const result = renderer
    .create(
      <PaymentSelectorComponent
        loading={true}
        paymentMethods={paymentMethods}
      />
    )
    .toJSON();

  // Assert
  expect(result).toMatchSnapshot();
});

test("display loading message", () => {
  // Arrange
  const loading = true;

  // Act
  const { getByTestId } = render(
    <PaymentSelectorComponent loading={loading} paymentMethods={[]} />
  );

  // Assert
  expect(getByTestId("payment-selector-loading")).toBeDefined();
});

test("without payment methods", () => {
  // Arrange
  const paymentMethods = [];

  // Act
  const { getByTestId } = render(
    <PaymentSelectorComponent loading={false} paymentMethods={paymentMethods} />
  );

  // Assert
  expect(getByTestId("payment-selector-no-methods")).toBeDefined();
});

test("without payment methods", () => {
  // Arrange
  const paymentMethods = [paymentMethodFactory.visa()];

  // Act
  const { getAllByTestId } = render(
    <PaymentSelectorComponent loading={false} paymentMethods={paymentMethods} />
  );

  // Assert
  expect(getAllByTestId("payment-selector-method")).toHaveLength(1);
});
