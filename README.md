# Introduction aux tests unitaires

IUT Robert Schuman - 2022-2023

## Supports de cours

- [Session #1](presentation_1.html)
- [Session #2](presentation_2.html)

## Exercices

- [Exercices #1](tp_1.pdf)
- [Exercices #2](tp_2.pdf)

## Rapports de test

- [Jest](coverage/)

## Notes

Le code source est disponible [ici](https://gitlab.com/cours-iut/2023)

Technologies utilisées :

- [Asciidoc](https://asciidoc.org/) pour les slides et les exercices
- [Asciidoctor Reveal.js](https://docs.asciidoctor.org/reveal.js-converter/latest) et [Reveal.js](https://revealjs.com/) pour les slides
- [Asciidoctor PDF](https://docs.asciidoctor.org/pdf-converter/latest/) pour les exercices
- [Markdown](https://fr.wikipedia.org/wiki/Markdown) pour ce README
- [Pandoc](https://pandoc.org/) et [markdowncss.github.io](http://markdowncss.github.io/) pour le style de la page
- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/)
- [Jest](https://jestjs.io/) pour les tests
- [Gitlab](https://gitlab.com), Gitlab CI et Gitlab Pages
